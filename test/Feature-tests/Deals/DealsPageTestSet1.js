import { Selector } from 'testcafe';
import config from "../../../config.json";
import { ClickOnButton,HoverOnButton ,AssertLink,AssertElementExists,AssertElementNotExists,typeText} from '../../../helpers/helper';

import HeaderElements from '../../../page objects/Reusable components/Header'
import OnboardingScreen from '../../../page objects/pages/Onboarding/OnboardingScreen'
import LoginScreen from '../../../page objects/pages/Onboarding/LoginScreen'
import DealsScreen from '../../../page objects/pages/Deals/DealsScreen'

var faker = require('faker');

fixture `Pipedrive Onboarding page and logging in`
    .page `${config.baseUrl}`
    .beforeEach(async t => {

        await t.maximizeWindow()
        
        //Assert that onboarding page is opened correctly
        await AssertLink(config.baseUrl)
        
        //Clicking on Login Button to Open Login Page
        await ClickOnButton(HeaderElements.LoginButton)

        //Asserting Login Page
        await AssertLink(LoginScreen.LoginPageURl)

        //Enter Email Password in Email password field which is residing in config file
        await LoginScreen.InputEmailPassword(config.username,config.Password)

        //Click on RememberMe Checkbox
        await ClickOnButton(LoginScreen.RememberMeCheckbox)

        //CLick on Submit button
        await ClickOnButton(LoginScreen.SubmitButton)

        // Asserting that user have logged in to correct company
        await AssertLink(config.CompanyName) 

    })




    test(('Validate that the user is taken to deals screen '),async t =>{
        
        
       await  t.expect(DealsScreen.DealsHeader.textContent).contains('Deals');


    })

    test(("Validate that the Add deal modal is opening by clicking + Deal Button and also gets closed"),async t => {

        await ClickOnButton(DealsScreen.dealsButton)
        await AssertElementExists(DealsScreen.AddDealModal)
        await ClickOnButton(DealsScreen.ModalCrossButton)
        await AssertElementNotExists(DealsScreen.AddDealModal)
    })

    test(("Validate that the Add deal modal is opening by clicking + button in pipeline stages"),async t => {

        await ClickOnButton(DealsScreen.DealButtonPipeline)
        await AssertElementExists(DealsScreen.AddDealModal)
        await ClickOnButton(DealsScreen.ModalCrossButton)
        await AssertElementNotExists(DealsScreen.AddDealModal)

    })

    test("Validate that the errors are showing if Add Deal form is submitted empty",async t =>{

        await ClickOnButton(DealsScreen.dealsButton)
        await AssertElementExists(DealsScreen.AddDealModal)

        await ClickOnButton(DealsScreen.SaveButton)
        await AssertElementExists(DealsScreen.ContactPersonErrorMessage)
        await AssertElementExists(DealsScreen.OrganizationErrorMessage)
        await AssertElementExists(DealsScreen.TitleErrorMessage)



    })



    test("Validate that the form doesn't submit if any of error is showing",async t =>{

        await ClickOnButton(DealsScreen.dealsButton)
        await AssertElementExists(DealsScreen.AddDealModal)

        await typeText(DealsScreen.ContactPersonField,faker.name.findName())

        await ClickOnButton(DealsScreen.SaveButton)
        await AssertElementExists(DealsScreen.AddDealModal)
        await AssertElementExists(DealsScreen.OrganizationErrorMessage)





    })


    test("Validate that the deal is being created with contact person and organization and is validated ",async t =>{

        await ClickOnButton(DealsScreen.dealsButton)
        await AssertElementExists(DealsScreen.AddDealModal)

        let contactPerson = faker.name.findName()
        let organization = faker.company.companyName()
        let title = faker.company.bs()

        await typeText(DealsScreen.ContactPersonField,contactPerson)
      
        await typeText(DealsScreen.OrganizationField,organization)
        await t.click(DealsScreen.DealTitleField).pressKey('ctrl+a delete')
        await typeText(DealsScreen.DealTitleField,title)

        await ClickOnButton(DealsScreen.SaveButton)

        await DealsScreen.ValidateOrganization(organization)
        await DealsScreen.ValidateTitle(title)






    })

    test("Validate that the deal detailed page is opening",async t =>{

        await ClickOnButton(DealsScreen.dealsButton)
        await AssertElementExists(DealsScreen.AddDealModal)

        let contactPerson = faker.name.findName()
        let organization = faker.company.companyName()
        let title = faker.company.bs()

        await typeText(DealsScreen.ContactPersonField,contactPerson)
      
        await typeText(DealsScreen.OrganizationField,organization)
        await t.click(DealsScreen.DealTitleField).pressKey('ctrl+a delete')
        await typeText(DealsScreen.DealTitleField,title)

        await ClickOnButton(DealsScreen.SaveButton)

        await DealsScreen.ValidateOrganization(organization)
        await DealsScreen.ValidateTitle(title)

        await DealsScreen.OpenDetailedPage(title)
        await DealsScreen.ValidateDetailedPageTitle(title)






    })



    test("Create n number of deals and validate them and their detailed page, open pipeline page through shortcut ",async t =>{


        let randNum  = Math.floor(Math.random() * 10) + 2;

        for(let i  = 0 ; i < randNum ; i++){
        await ClickOnButton(DealsScreen.dealsButton)
        await AssertElementExists(DealsScreen.AddDealModal)

        let contactPerson = faker.name.findName()
        let organization = faker.company.companyName()
        let title = faker.company.bs()
    

        await typeText(DealsScreen.ContactPersonField,contactPerson)
      
        await typeText(DealsScreen.OrganizationField,organization)
        await t.click(DealsScreen.DealTitleField).pressKey('ctrl+a delete')
        await typeText(DealsScreen.DealTitleField,title)

       

        await ClickOnButton(DealsScreen.SaveButton)

        await DealsScreen.ValidateOrganization(organization)
        await DealsScreen.ValidateTitle(title)

        await DealsScreen.OpenDetailedPage(title)
        await DealsScreen.ValidateDetailedPageTitle(title)
        await t.pressKey('2')



        }


    })


    test("Create a simple deal with a random currency and value",async t =>{

        await ClickOnButton(DealsScreen.dealsButton)
        await AssertElementExists(DealsScreen.AddDealModal)

        let contactPerson = faker.name.findName()
        let organization = faker.company.companyName()
        let title = faker.company.bs()
        let dealVal = faker.datatype.number()
        let currency = ""

        dealVal = dealVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        await typeText(DealsScreen.ContactPersonField,contactPerson)
      
        await typeText(DealsScreen.OrganizationField,organization)
        await t.click(DealsScreen.DealTitleField).pressKey('ctrl+a delete')
        await typeText(DealsScreen.DealTitleField,title)
        await typeText(DealsScreen.dealValuefield,dealVal.toString())
        await ClickOnButton(DealsScreen.dealCurrencyDropdown)
        currency = await DealsScreen.updateCurrencyDropdown.innerText;
        
        
        await DealsScreen.ChooseAcurrencyAtRuntime()

        await ClickOnButton(DealsScreen.SaveButton)

        await DealsScreen.ValidateOrganization(organization)
        await DealsScreen.ValidateTitle(title)

        await DealsScreen.OpenDetailedPage(title)
        await DealsScreen.ValidateDetailedPageTitle(title)






    })

    