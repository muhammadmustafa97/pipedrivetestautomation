import {Selector, t } from 'testcafe';
import { ClientFunction } from 'testcafe';
import config from "../config.json"
import XpathSelector from '../utils/Xpath-selector';

//function to go to baseURL
export async function OpenWebsite(){
 await fixture `Casper Web Page`
    .page `${config.baseUrl}`;
}
//function to click on any Button
  export async function ClickOnButton(Button){
    await t
        .click(Button)
}

//function to Hover on any button
export async function HoverOnButton(Button){
    await t
        .hover(Button);
}
//function to assert links on top
export async function AssertLink(linkVal){
  const getLocation = ClientFunction(() => document.location.href.toString());
  await t.expect(getLocation()).contains(linkVal);
}

export async function AssertElementExists(Selector){


  await t.expect(Selector.exists).ok()
}

export async function AssertElementNotExists(Selector){


  await t.expect(Selector.exists).notOk()
}

export async function typeText(Selector,value){

  await t.typeText(Selector,value)
}