import { Selector,t } from "testcafe";
import xpathSelector from "../../../utils/xpath-selector";


class DealsScreen{

    constructor(){

        this.DealsHeader = Selector("h1[title='Deals']")

        this.DealButtonPipeline = xpathSelector("(//button[@class='cui4-button cui4-button--ghost'])[1]")
        this.dealsButton = Selector("button[aria-label='Add deal']")
        this.AddDealModal = Selector(".cui4-modal__wrap")

        this.ContactPersonField = xpathSelector("//div[contains(text(),'Contact person')]//following::input")
        this.ContactSuggesstionBox = Selector(".cui4-option.ff_v601__EntitySuggestions__addNew__2Gf")
        this.OrganizationField = xpathSelector("//div[contains(text(),'Organization')]//following::input")
        this.DealTitleField = Selector("div[class='cui4-input ff_editMode'] input[type='text']")

        this.dealValuefield = Selector("div[data-test-type='monetary'] div input[type='text']")
        this.dealCurrencyDropdown = xpathSelector("(//span[@aria-label='open menu'])[1]")
        this.dealCurrencyDropdownSearchField = Selector("div[id^='downshift-'][id$='-item-']")

        this.AddproductsBtn = Selector(".client-components-Products-AddProducts-AddProducts__addProducts--36IOL")
        this.ProductsModal - xpathSelector("//span[normalize-space()='PRODUCTS']")

        this.Pipelinedropdown = xpathSelector("//span[@aria-label='open menu']//span[contains(text(),'Pipeline')]")
        
        this.ClosingdateField = Selector("input[placeholder='MM/DD/YYYY']")

        this.ownergroupDropdown = Selector(".cui4-button.ff_v601__VisibilitySwitch__select__2r7")
        
        this.PhonenumberField = xpathSelector("(//input[@type='text'])[6]")
        this.PhoneNumberDropdown = xpathSelector("(//span[@aria-label='open menu'])[3]")

        this.EmailField = xpathSelector("(//span[@aria-label='open menu'])[3]")
        this.EmailFieldDropdown = xpathSelector("(//span[@aria-label='open menu'])[4]")
        
        
        this.ProductsNameField = Selector("input[data-test='product-input']")
        this.ProductPrice = Selector("input[value='0'][type='number']")
        this.ProductQuantity = Selector("input[value='1']")
        this.ProductAmount = Selector("div[class='client-components-Products-ProductsFormRow-ProductsFormRow__amount--OxuX_'] input[value='0']")
        this.ProductAmountTotal = Selector(".client-components-Products-ProductsForm-ProductsForm__totalValue--3cAoY")

        this.DealsLimitButton = Selector(".cui4-button.cui4-button--ghost-alternative.cui4-button--s.sc-pjumZ.gUhPC")


        this.SaveButton = Selector("button[data-test='add-modals-save'] span")
        this.cancelButton = Selector("button[data-test='add-modals-cancel-button']")

        this.ModalCrossButton = Selector(".cui4-button.cui4-button--ghost.cui4-button--s.cui4-modal__close")

        this.updateCurrencyDropdown = Selector("div[class='cui4-select ff_v601__InputSelectCompound__select__1hE'] span[aria-label='open menu'] span")
        //Error Messages
        this.ContactPersonErrorMessage = xpathSelector("//div[@data-test-type='person']//div//div//div[@class='cui4-input__message'][normalize-space()='A person or organization is required']")
        this.OrganizationErrorMessage = xpathSelector("//div[@data-test-type='organization']//div//div//div[@class='cui4-input__message'][normalize-space()='A person or organization is required']")
        this.TitleErrorMessage = xpathSelector("//div[contains(text(),'Title is required')]")
    }


    async ValidateTitle(title){

        await  t.expect(xpathSelector("//span[normalize-space()='"+title+"']").exists).ok()
    }
    async ValidateOrganization(organization){

        await  t.expect(xpathSelector("//div[normalize-space()='"+organization+"']").exists).ok()
    }

    async ValidateDetailedPageTitle(title){

        await t.expect(xpathSelector("//a[normalize-space()='"+ title +"']").exists).ok()
    }

    async OpenDetailedPage(title){

        await t.click(xpathSelector("//span[normalize-space()='"+title+"']/parent::a"))
    }
    async ChooseAcurrencyAtRuntime(){

        let generateArand = Math.floor(Math.random() * 182) + 1;
        await t.click(Selector("div[id^='downshift-'][id$='-item-"+generateArand+"']"))
    }
}

export default new DealsScreen()