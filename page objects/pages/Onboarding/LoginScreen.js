import { Selector,t } from "testcafe";
import xpathSelector from "../../../utils/xpath-selector";

class LoginScreen{

    constructor(){
        
        this.LoginPageURl = "/auth/login"

        this.EmailField =  Selector("#login")
        this.PasswordField  = Selector("#password")
        
        this.RememberMeCheckbox = Selector(".styled-checkbox__label")

        this.SubmitButton = Selector("button[name='submit']")
    }

    async InputEmailPassword(Email,Password){

        await t.typeText(this.EmailField,Email)
        await t.typeText(this.PasswordField,Password)
    }

}

export default new LoginScreen()