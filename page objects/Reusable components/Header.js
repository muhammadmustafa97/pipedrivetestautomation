import XpathSelector from "../../utils/Xpath-selector";
import { Selector,t } from "testcafe";


class Header{

    constructor(){
    
    //Retrieval of objects
    
    
    //Retreive different categories elements which are available on header of every page
        //Pipedrive Logo
        this.PipedriveLogo = Selector("a[class='puco-link puco-link--display-inline puco-link--no-spacing']") 
        //Login Button  
        this.LoginButton = XpathSelector("//span[normalize-space()='Login']")
  
    
    }


}

export default new Header();
